FROM alpine

ENV TERRAFORM_VERSION=1.1.1
ENV TERRAGRUNT_VERSION=0.35.16

# System packages
RUN apk add bash jq openssl git curl aws-cli

# Terraform & Terragrunt
RUN git clone https://github.com/tfutils/tfenv.git ~/.tfenv && ln -s ~/.tfenv/bin/* /usr/local/bin
RUN git clone https://github.com/cunymatthieu/tgenv.git ~/.tgenv && ln -s ~/.tgenv/bin/* /usr/local/bin
RUN tfenv install $TERRAFORM_VERSION && tfenv use $TERRAFORM_VERSION
RUN tgenv install $TERRAGRUNT_VERSION && tgenv use $TERRAGRUNT_VERSION

CMD ["terragrunt"]